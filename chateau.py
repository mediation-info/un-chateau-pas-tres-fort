# On représente le château par une liste de couples de listes : la première composante du couple correspond au contenu de la salle, la deuxième composante aux pièces voisines

TABLEAU = "tableau"
TORCHE = "torche"
COFFRE = "coffre"
FENETRE = "fenêtre"

INDICE_CONTENU = 0
INDICE_VOISINES = 1

# Représentation du château
# Structure pour des étudiants ne connaissant pas le monde objet
# chateau = tableau de salles
# salles :
#   - identidifées par leur numéro (indice dans le tablau décrivant le château)
#   - définies par 1 paire :
#       - première composante : la liste des "objets" dans la pièce
#       - deuxième composante : les numéros des salles accessibles directement
chateau = [None] * 10
chateau[0] = ([TABLEAU, TORCHE], [1, 2])
chateau[1] = ([TABLEAU, TORCHE], [1, 5])
chateau[2] = ([TABLEAU], [0, 7])
chateau[3] = ([COFFRE], [1, 4, 6])
chateau[4] = ([TABLEAU], [1, 7])
chateau[5] = ([FENETRE, TORCHE], [2, 4, 6, 9])
chateau[6] = ([FENETRE], [4, 7, 8])
chateau[7] = ([COFFRE, TORCHE], [5, 7])
chateau[8] = ([FENETRE, TABLEAU], [9])
chateau[9] = ([TORCHE], [8])

# liste de fonctions pour savoir si une pièce contient un objet donné
def contient_coffre(piece):
    return piece_contient(piece, COFFRE)

def contient_tableau(piece):
    return piece_contient(piece, TABLEAU)

def contient_torche(piece):
    return piece_contient(piece, TORCHE)

def contient_fenetre(piece):
    return piece_contient(piece, FENETRE)

def piece_contient(piece, objet):
    return objet in chateau[piece][INDICE_CONTENU]

def poursuivre_chemin(chemin):
    """
    Fonction permettant, à partir d'un chemin menant à un salle, de construire
    la liste des chemins menant aux salles voisines.
    """
    assert chemin != []
    derniere_salle = chemin[-1]
    resultat = []
    for piece_suivante in chateau[derniere_salle][INDICE_VOISINES]:
        suite = chemin + [piece_suivante]
        resultat.append(suite)
    return resultat


def poursuivre_chemins(chemins):
    """
    Fonction permettant, à partir d'une liste de chemins, de construire la liste des
    chemins issus de ces préfixes et d'une étape de plus
    """
    resultat = []
    for chemin in chemins:
        suite = poursuivre_chemin(chemin)
        resultat = resultat + suite
    return resultat


def progres(chemins):
    """
    Fonction permettant de savoir si parmi tous les chemins obtenus, au moins 1
    permet d'atteindre une salle encore jamais atteinte
    """
    avant = set()
    apres = set()
    for chemin in chemins:
        avant = avant | set(chemin[:-1])
        apres.add(chemin[-1])
    return apres - avant != set()


def atteignable(propriete, chemins):
    """
    Fonction permettant, à partir d'une liste de chemins, de savoir si l'un de ces
    chemins passe par une salle vérifiant une propriété donnée
    """
    for chemin in chemins:
        for salle in chemin:
            if propriete(salle):
                return True
    return False


def accessible(propriete, depart = 0):
    """
    Fonction permettant de savoir si une salle vérifiant une propriété donnée est
    accessible depuis le départ
    """
    chemins = [[depart]]
    while not atteignable(propriete, chemins):
        chemins = poursuivre_chemins(chemins)
        if not progres(chemins):
            return False
    return True

def fatalement(propriete, depart = 0):
    """
    Fonction permettant de savoir si tout chemin finit par passer par une salle
    vérifiant une propriété donnée
    """
    chemins = [[depart]]
    while not chemins == []:
        chemins_restant = []
        for chemin in chemins:
            if not propriete(chemin[-1]):
                if chemin[-1] not in chemin[:-1]:
                    chemins_restant.append(chemin)
                else:
                    return False
        chemins = poursuivre_chemins(chemins_restant)
    return True

def existe_chemin_sur(propriete, depart = 0):
    """
    Fonction permettant de savoir s'il existe un chemin dont tous les états vérifient
    une propriété donnée
    """
    chemins =[[depart]]
    while not chemins == []:
        chemins_restant = []
        for chemin in chemins:
            if propriete(chemin[-1]):
                if chemin[-1] in chemin[:-1]:
                    return True
                else:
                    chemins_restant.append(chemin)
        chemins = poursuivre_chemins(chemins_restant)
    return False

def toujours(propriete, depart = 0):
    """
    Fonction permettant de savoir si tous les états de tous les chemins vérifient
    une propriété donnéee
    """
    chemins = [[depart]]
    while not chemins == []:
        chemins_restant = []
        for chemin in chemins:
            if not propriete(chemin[-1]):
                return False
            if not chemin[-1] in chemin[:-1]:
                chemins_restant.append(chemin)
        chemins = poursuivre_chemins(chemins_restant)
    return True

def chemins_n_dep(nb_deplacements, depart):
    """
    Fonction calculant tous les chemins de longueur nb_deplacements
    """
    chemins = [[depart]]
    for dep in range(nb_deplacements):
        chemins = poursuivre_chemins(chemins)
    return chemins

def chemins_n_dep_au_plus(nb_deplacements, depart):
    """
    Fonction calculant tous les chemins d'au plus nb_deplacements
    """
    chemins = [[depart]]
    retour = [[depart]]
    for dep in range(nb_deplacements):
        chemins = poursuivre_chemins(chemins)
        retour += chemins
    return retour

def accessible_en_n_exactement(nb_deplacements, propriete, depart = 0):
    """
    Fonction permettant de savor si une salle vérifiant une propriété donnée
    est accessible en exactement nb_deplacements
    """
    resutat = False
    chemins = chemins_n_dep(nb_deplacements, depart)
    for chemin in chemins:
        if propriete(chemin[-1]):
            # print(chemin)
            resultat = True
    return resultat

def accessible_en_n_au_plus(nb_deplacements, propriete, depart = 0):
    """
    Fonction permettant de savor si une salle vérifiant une propriété donnée
    est accessible en au plus nb_deplacements
    """
    resutat = False
    chemins = chemins_n_dep_au_plus(nb_deplacements, depart)
    for chemin in chemins:
        if propriete(chemin[-1]):
            # print(chemin)
            resultat = True
    return resultat


def requiert_param_num(operateur):
    """
    Fonction permettant de savoir si un opérateur temporel (parmi ceux ci-dessus)
    requiert un paramètre numérique ou pas
    """
    return operateur == accessible_en_n_exactement or operateur == accessible_en_n_au_plus

# Q1. Je peux accéder à un trésor
accessible(contient_coffre)
# ou
accessible(lambda piece: contient_coffre(piece))
# Q2. Je peux atteindre une pièce ou je peux me remplir les poches (de trésor) tout en admirant un tableau.
accessible(lambda piece: contient_coffre(piece) and contient_tableau(piece))
# Q3. Je peux aller dans une pièce où je pourrai correctement admirer un tableau (pour l’admirer il faut une source de lumière, il y en a deux possibles).
accessible(lambda piece: contient_coffre(piece) and (contient_torche(piece) or contient_fenetre(piece)))
# Q4. Toute balade dans le château permet de voir au moins une fenêtre.
fatalement(lambda piece: contient_fenetre(piece))
# Q5. Je peux me balader dans uniquement des pièces avec des torches.
existe_chemin_sur(lambda piece: contient_torche(piece))
# Q6. Je vais forcément me balader dans uniquement des pièces avec des torches.
toujours(lambda piece: contient_torche(piece))
## Q7. Je peux arriver devant un tableau en exactement 3 déplacements.
accessible_en_n_exactement(3, lambda piece: contient_tableau(piece))
# Q8. Je peux arriver devant un tableau en 3 déplacements maximum.
accessible_en_n_au_plus(3, lambda piece: contient_tableau(piece))
# Q9. Quoi qu’on fasse, on verra toujours une torche, un portraît ou une fenêtre dans sa pièce.
toujours(lambda piece: contient_torche(piece) or contient_fenetre(piece) or contient_tableau(piece))
# Q10. Je ne peux pas me retrouver devant un tableau sans torche.
toujours(lambda piece: contient_torche(piece) or not contient_tableau(piece))
## Q11. Si je me promène dans le château, à partir d’un moment je ne verrai plus jamais de tableau.
# Q12. Il existe un chemin (infini) qui ne passe jamais par le trésor
existe_chemin_sur(lambda piece: not contient_coffre(piece))
## Q13. Je peux me balader et ne plus voir de torches après un certain moment


def menu():
    """
    Affichage du menu principal
    """
    print("Choix de l'opérateur : ")
    print("0. quitter")
    print("1. accessible")
    print("2. fatalement")
    print("3. existe chemin sûr")
    print("4. toujours")
    print("5. accessible en n exactement")
    print("6. accessible en n au plus")

def choisir_operateur():
    """
    Saisie de l'opérateur temporel
    """
    operateurs = [None, accessible, fatalement, existe_chemin_sur, toujours, accessible_en_n_exactement, accessible_en_n_au_plus]
    choix = None
    while choix is None:
        menu()
        saisie = input("Opérateur : ")
        try:
            num = int(saisie)
            if num < 0 or num > 6:
                raise Exception()
            choix = num
        except:
            print("Saisie incorrecte")
    return operateurs[num]

def saisie_propriete():
    """
    Saisie de la propriété devant être vérifié par le ou les états de la balade
    """
    print("Entrez la propriété à vérifier : ")
    print("Exemple 1 : torche")
    print("Exemple 2 : torche or not tableau")
    print("Exemple 3 : numero == 3")
    correct = False
    while not correct:
        propriete = input()
        propriete = propriete.replace("numero", "piece")
        propriete = propriete.replace("numéro", "piece")
        propriete = propriete.replace(TORCHE, "contient_torche(piece)")
        propriete = propriete.replace(TABLEAU, "contient_tableau(piece)")
        propriete = propriete.replace(FENETRE, "contient_fenetre(piece)")
        propriete = propriete.replace("fenetre", "contient_fenetre(piece)")
        propriete = propriete.replace(COFFRE, "contient_coffre(piece)")
        texte_fonction = "lambda piece: (" + propriete + ")"
        try:
            fonction = eval(texte_fonction)
            fonction(0) # juste pour vérifier l'existence des opérateurs...
            correct = True
        except:
            print("Formule incorrecte")
    return fonction

def lire_naturel(invite = "Saisissez un entier positif"):
    """
    Fonction permettant de saisir - avec contrôle - un entier naturel
    """
    correct = False
    num = -1
    while not correct:
        print(invite, end=" : ")
        try:
            num = int(input())
            if num < 0:
                raise Exception()
            correct = True
        except:
            print("Saisie incorrecte")
    return num

def main():
    """
    Programme principal
    """
    termine = False
    while not termine:
        operateur = choisir_operateur()
        if operateur is not None:
            propriete = saisie_propriete()
            if not requiert_param_num(operateur):
                resultat = operateur(propriete)
            else:
                num = lire_naturel()
                resultat = operateur(num, propriete)
            if resultat:
                print("\nLa propriété est vraie\n")
            else:
                print("\nLa propriété est fausse\n")
            input('Appuyez sur la touche "entrée" pour continuer')
        else:
            termine = True

if __name__ == "__main__":
    main()
    
