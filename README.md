# Un château pas très fort

Petit programme Python fournissant les bases pour l'activité [Un
château pas très fort](https://members.loria.fr/MDuflot/files/med/chateau.html)
proposée par Marie Duflot-Kremer.

Il s'agit d'une activité de médiation scientifique permettant
d'initier à la notion de vérification de propriétés temporelles.

